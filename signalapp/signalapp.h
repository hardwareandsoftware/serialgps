/** @file signalapp.h
 *  @brief Arquivo de cabeçalho do signalapp.c
 */ 
#ifndef SIGNALAPP_H_
#define SIGNALAPP_H_

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <syslog.h>

/** @function 
 *  Essa função faz o handler da sinal SIGINT
 *  @author Tiago Sousa Rocha
 *  @param [in] int s
 *  @return void
 */
void signalHandlerSIGINT (int s);

/** @function 
 *  Essa função faz o handler da sinal SIGPIPE
 *  @author Tiago Sousa Rocha
 *  @param [in] int s
 *  @return void
 */
void signalHandlerSIGPIPE (int s);

/** @function 
 *  Essa função faz o handler da sinal SIGCHLD
 *  @author Tiago Sousa Rocha
 *  @param [in] int s
 *  @return void
 */
void signalHandlerSIGCHLD (int s);

/** @function 
 *  Essa função faz o handler da sinal SIGALRM
 *  @author Tiago Sousa Rocha
 *  @param [in] int s
 *  @return void
 */
void signalHandlerSIGALRM (int s);

#endif  // SIGNALAPP_H_