#include "signalapp.h"

/* SIGINT  */
void signalHandlerSIGINT (int s) {
    fprintf(stdout, "DEBUG: signalHandler_SIGINT ( %d ) \n", s);
    syslog(LOG_INFO, "==== DEBUG: signalHandler_SIGINT ( %d ) \n", s);
    sleep(1);
    /* Executa o necessário antes de finalizar o software */
    // TODO: Preparação para finalizar o aplicativo
    
    syslog(LOG_INFO, "==== DEBUG: Finalizando a aplicação  ====\n", s);    
    exit(EXIT_SUCCESS);
};

void signalHandlerSIGPIPE (int s) {
    fprintf(stdout, "DEBUG: signalHandler_SIGPIPE ( %d ) \n", s);
    sleep(1);
};

void signalHandlerSIGCHLD (int s) {
    int status;
    wait(&status);
    fprintf(stdout, "DEBUG: signalHandler_SIGCHLD ( %d ) \n", s);
    sleep(1);
};

void signalHandlerSIGALRM (int s) {
    fprintf(stdout, "DEBUG: signalHandler_SIGALRM ( %d ) \n", s);
    sleep(1);
};


/*

SIGHUP 	1 	Hangup (POSIX) 	Sim
SIGINT 	2 	Interrupt (ANSI) – O mesmo que CTRL+C 	Sim
SIGQUIT 	3 	Quit (POSIX) 	Sim
SIGILL 	4 	Illegal Instruction (ANSI) 	Sim
SIGTRAP 	5 	Trace trap (POSIX) 	Sim
SIGABRT 	5 	Trace trap (POSIX) 	Sim
SIGIOT 	6 	Abort (ANSI) 	Sim
SIGIOT 	6 	IOT trap (4.2 BSD) 	Sim
SIGBUS 	7 	BUS error (4.2 BSD) 	Sim
SIGFPE 	8 	Floating-Point arithmetic Exception (ANSI) 	Sim
SIGKILL 	9 	Kill, unblockable (POSIX) 	Não
SIGUSR1 	10 	User-defined signal 1 	Sim
SIGEGV 	11 	Segmentation Vionation (ANSI) 	Sim
SIGUSR2 	12 	User-defined signal 2 	Sim
SIGPIPE 	13 	Broken pipe (POSIX) 	Sim
SIGALRM 	14 	Alarm clock (POSIX) 	Sim
SIGTERM 	15 	Termination (ANSI) 	Sim
SIGTKFLT 	16 	Stack fault 	Sim
SIGCHLD 	16 	Stack fault 	Sim
SIGCLD 	17 	Child status has changed (POSIX) 	Sim
SIGCONT 	18 	Continue (POSIX) 	Sim
SIGSTOP 	19 	Stop, unblockable (POSIX) 	Não
SIGTSTP 	20 	Keyboard stop (POSIX) – O mesmo que CTRL+Z 	Sim
SIGTTIN 	21 	Background read from tty (POSIX) 	Sim
SIGTTOU 	22 	Background write from tty (POSIX) 	Sim
SIGURG 	23 	Urgent condition on socket (4.2 BSD) 	Sim
SIGXCPU 	24 	CPU limit exceeded (4.2 BSD) 	Sim
SIGXFSZ 	25 	File size limie exceeded (4.2 BSD) 	Sim
SIGVTALRM 	26 	Virtual Time Alarm (4.2 BSD) 	Sim
SIGPROF 	27 	Profiling alarm clock (4.2 BSD) 	Sim
SIGWINCH 	28 	Window size change (4.3 BSD, Sun) 	Sim
SIGIO 	28 	Window size change (4.3 BSD, Sun) 	Sim
SIGPOLL 	29 	I/O now possible (4.2 BSD) 	Sim
SIGPOLL 	29 	Pollable event ocurred (System V) 	Sim
SIGPWR 	30 	Power failure restart (System V) 	Sim
SIGSYS 	31 	Bad system call 	Sim



Como exemplos, podemos citar os seguintes sinais:

    SIGHUP: Utilizado quando um terminal de controle é fechado;
        O comportamento default é parar a execução, embora muitos deamons realizem operações de reload ao receber este sinal;
    SIGTERM: Informa que o programa deve terminar a execução;
        Possui o mesmo comportamento default que o SIGHUP;
        É o sinal padrão enviado pelos comandos kill e killall;
    SIGINT: Informa que o programa recebeu uma interrupção pelo terminal de controle;
        Muito semelhante ao SIGTERM;
        Ocorre ao pressionar CTRL+C no terminal onde o processo está executando em foreground;
    SIGKILL: Finaliza forçadamente o programa;
        É parecido com o SIGINT e o SIGTERM, mas não pode ser sobrescrito;
    SIGTSTP: Informa que o programa deve ser pausado (colocado em estado suspended);
        Ocorre ao pressionar CTRL+Z no terminal em um processo em foreground;
    SIGSTOP: Pausa forçadamente um processo;
        É semelhante ao SIGTSTP, porém, ao contrário do primeiro, não pode ser sobrescrito;
    SIGCONT: Retorna um processo pausado (suspended) para a fila de prontos (estado running);
    SIGCHLD: Informa a um processo pai que algum filho terminou a execução, foi suspendido (suspended) ou resumido (voltando ao estado running).


//*/