/** @file serialgps.h
 *  @brief Arquivo de cabeçalho do serialgps.c
 *  @author Tiago Sousa Rocha
 */
#ifndef SERIALGPS_H_
#define SERIALGPS_H_

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <syslog.h>
#include <errno.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <signal.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <time.h>
#include "gps/gps.h"
#include "signalapp/signalapp.h"
#include "protocolos/nmea/nmea.h"
#include "tipos.h"

#define SZ_MAX_CONFIG_FILENAME              1024
#define DEF_READ_GPSDATA_INTERVAL           10
#define DEF_SERIALPORT_FILENAME             "/dev/ttyUSB2"
#define DEF_FILENAME_GPS_MEMORYMAPPING      "/tmp/gps"
#define DEF_SHARING_GPSDATA_INTERVAL        30
#define OPT_ARGS_OPTIONS                    "DtiTMd:"
#define DEF_INTERVAL_CHK_SERIALPORT         20
#define DEF_INTERVAL_ACQUISITION_SERIALDATA 10

// Memory Mapping
#define RWX_UGO 0x100600 | (S_IRWXU | S_IRWXG | S_IRWXO)



/** @struct TApplicationConfiguration
 *  Essa estrutura de dados é responsável por armazenar os parâmetros de configuração da aplicação.
 * 
 *  @author Tiago Sousa Rocha
 */
typedef struct {
    unsigned char cfgfilename[SZ_MAX_CONFIG_FILENAME+1];
    int readGpsDataInterval; /*!< É o intervalo de leitura dos dados recebidos pelo canal serial RS-232 enviados pelo dispositivo GPS. <br /> O valor default é <strong>10 segundos</strong>. */
    int gpsDataAcquisitionInterval; /*!<  */
    int sharingGpsDataInterval; /*!< É o intervalo de compartilhameneto dos dados GPS através do Memory Mapping. <br />O valor default é <strong>30 segundos</strong>. */
    unsigned char mmSharedFile[SZ_MAX_CONFIG_FILENAME+1]; /*!< É o arquivo de compartilhamento do objeto TGPS através do Memory Mapping. */
    unsigned char deviceSerialFilename[SZ_MAX_CONFIG_FILENAME+1]; /*!< É o arquivo de comunicação com o GPS serial. Ex: <strong>/dev/ttyUSB0</strong>. */
    struct {
        unsigned cfg : 1; /*!< Flag que habilita o debug das configurações do software */
    } debug;
} TApplicationConfiguration;

/** @function 
 *  Essa função exibe um Help do software, com as mensagens que ajudam o usuário a executar o software com os parâmetros correto, 
 * ou pelo menos com o mínimo de parâmentros necessários.
 *  @author Tiago Sousa Rocha
 *  @param [in] char applcationname
 *  @return void 
 */
void showHelp(
    char *applicationname /*!< É o nome dado ao software. */
);

/** @function 
 *  @brief Essa função processa os argumentos de linha de comando passados para o software.
 *  <br />Dentre os parâmetros a serem utilizados estão:
 *  <br /><strong>-D:</strong> Arquivo do dispositivo serial. Exemplo: <strong>/dev/ttyACM0</strong>
 *  <br /><strong>-t:</strong> Intervalo de leitura dos dados do GPS. Default: <strong>10 segundos</strong>
 *  <br /><strong>-T:</strong> Intervalo de compartilhamento dos dados do GPS através de Memory Mapping. Default: <strong>30 segundos</strong>
 *  <br /><strong>-M:</strong> Arquivo a ser utilizado para compartilhamento através de Memory Mapping. Default: <strong>/sys/program/gps</strong>
 * 
 *  @author Tiago Sousa Rocha
 *  @param [in] int argc
 *  @param [in] char **argv 
 *  @return void
 */
void processArgs(
	int argc /*!< É a quantidade de argunmentos passados para o software.*/, 
	char **argv /*!< É o vetor de caracteres contendo os argumentos passados para o software. */,
    TApplicationConfiguration *appconfig /*!< É a esturuta de configuração da aplicação. */
);

void timerMMGPSHandler(size_t timer_id, void * user_data);

#endif // SERIALGPS_H_