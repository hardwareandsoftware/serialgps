#=============================================================================================
#|
#|       Author: Tiago Sousa Rocha
#|
# ==============================================================================================
EXENAME=serialgps
INC=-I/usr/include
CC=gcc
LDFLAGS=-lpthread
CFLAGS_DEBUG=$(CFLAGS) -Wall
#CPPFLAGS=-DRUN_AS_DAEMON
CPPFLAGS=-DDEBUG_GPS -DDEBUG_CFG -DTIMERSERIAL
LIBS=-L/usr/lib

OBJ=serialgps.o signalapp/signalapp.o gps/gps.o protocolos/nmea/nmea.o mytimer/mytimer.o
#OBJ=serialgps.o signalapp/signalapp.o 

.PHONY: all run clean force

all: $(EXENAME)

$(EXENAME): $(OBJ)
	$(CC) -o $(EXENAME) $(OBJ) $(LDFLAGS) $(LIBS) $(CPPFLAGS)

mytimer.o: mytimer/mytimer.c mytimer/mytimer.h tipos.h
	$(CC) $(CFLAGS_DEBUG) $(CPPFLAGS) $(INC) -c -o $@ $<

signalapp.o: signalapp/signalapp.c signalapp/signalapp.h tipos.h
	$(CC) $(CFLAGS_DEBUG) $(CPPFLAGS) $(INC) -c -o $@ $<

gps.o: gps/gps.c gps/gps.h mytimer/mytimer.h tipos.h
	$(CC) $(CFLAGS_DEBUG) $(CPPFLAGS) $(INC) -c -o $@ $<

nmea.o: protocolos/nmea/nmea.c protocolos/nmea/nmea.h tipos.h
	$(CC) $(CFLAGS_DEBUG) $(CPPFLAGS) $(INC) -c -o $@ $<

serialgps.o: serialgps.c serialgps.h tipos.h gps/gps.h signalapp/signalapp.h
	$(CC) $(CFLAGS_DEBUG) $(CPPFLAGS) $(INC) -c -o $@ $<


run: all
	./$(EXENAME) -d -t 33 -T 34 -D /dev/ttyUSB0 -M /sys/cfg/gps

clean:
	rm -rf ./*.o
	rm -rf ./signalapp/signalapp/*.o
	rm -rf ./gps/*.o
	rm -rf ./protocolos/*.o
	rm -rf ./mytimer/*.o
	rm -rf ./protocolos/nmea/*.o

force:
	make clean
	make all
	make run

mrproper:
	rm -rf $(EXENAME)
