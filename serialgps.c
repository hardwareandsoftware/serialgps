/** @file serialgps.c
 *  Ele deve ser executado como serviço e coletará todos os dados de Geo Localização recebidos através de um
 * dispositivo GPS utilizando a comunicação serial RS-232.
 * 
 *  @author Tiago Sousa Rocha
 *  @brief Este é o arquivo principal do software.
 */
#include "serialgps.h"


#define SYSLOG_APPLICATION_NAME    	"serialgps"
#define APPLICATION_NAME    		"Serial GPS Service"
#define VERSION_NAME        		"1.00-r00"
#define MINIMAL_ARGS				2
#define MANDATORY_ARGS				1
//#define RUN_AS_DAEMON
#define FILE_CONFIGURATION			"/etc/serialgps.conf"
#define MAX_SIZE_STRING				254
#define COUNT_NMEA					5


static pthread_attr_t  attr;	/*!< Atributos da thread */

int main(int argc, char **argv) 
{
	TApplicationConfiguration appconfig;
    TGPSDevice *device;
    pthread_t thSerial;
	int _timeReadSerialData = 0;
	// Memory Mapped 


	// Adiciona o handler para o sinal SIGINT
    if (signal(SIGINT, signalHandlerSIGINT) == SIG_ERR) {
        syslog(LOG_INFO,"==== ERROR: Falha ao definir o signal SIGINT.\n");
        return EXIT_FAILURE;
    } else syslog(LOG_INFO,"==== DEBUG: signal(SIGINT) defined for signalHandlerSIGINT\n");

	// Adiciona o handler para o sinal SIGPIPE
    if (signal(SIGPIPE, signalHandlerSIGPIPE) == SIG_ERR) {
        syslog(LOG_INFO,"==== ERROR: Falha ao definir o signal SIGPIPE.\n");
        return EXIT_FAILURE;
    } else syslog(LOG_INFO,"==== DEBUG: signal(SIGPIPE) defined for signalHandlerSIGPIPE\n");

	// Adiciona o handler para o sinal SIGCHLD
    if (signal(SIGCHLD, signalHandlerSIGCHLD) == SIG_ERR) {
        syslog(LOG_INFO,"==== ERROR: Falha ao definir o signal SIGCHLD.\n");
        return EXIT_FAILURE;
    } else syslog(LOG_INFO,"==== DEBUG: signal(SIGCHLD) defined for signalHandlerSIGCHLD\n");

	// Adiciona o handler para o sinal SIGALRM
    if (signal(SIGALRM, signalHandlerSIGALRM) == SIG_ERR) {
        syslog(LOG_INFO,"==== ERROR: Falha ao definir o signal SIGALRM.\n");
        return EXIT_FAILURE;
    } else syslog(LOG_INFO,"==== DEBUG: signal(SIGALRM) defined for signalHandlerSIGALRM\n");

	// Trata os argumentos
	opterr = 0;
	processArgs(argc, argv, &appconfig);

	// Tenta executar o software como Daemon
	#ifdef RUN_AS_DAEMON
	if (daemon(1, 1) < 0) {
		syslog(LOG_INFO,"==== ERROR: Falha ao tentar executar como Daemon!\n");
		return EXIT_FAILURE;
	}
	#endif

	// Abre o Syslog para Debug e Informações do sistema
	openlog(SYSLOG_APPLICATION_NAME, LOG_PID, LOG_DEBUG);
	syslog(LOG_INFO, "==== Inicializando %s versão: %s ====", APPLICATION_NAME, VERSION_NAME);

	syslog(LOG_INFO, "==== DEBUG_MAIN: device is NULL \n");
	if ((device = calloc(1, sizeof(TGPSDevice))) != NULL) {
		syslog(LOG_INFO, "==== DEBUG_MAIN: device alocado \n");
		clearParserGPSSerial(device);
		device->flags.readyReadGPSData = 1;
		device->gpsDataAcquisitionInterval = appconfig.gpsDataAcquisitionInterval;
		strcpy(device->serialport, appconfig.deviceSerialFilename);

		if (stat(appconfig.mmSharedFile, &device->gps.sb) != 0) {
			syslog(LOG_INFO, "==== DEBUG_MM: Stats do Arquivo %s falhou! ERROR: errno=%d\n", appconfig.mmSharedFile, errno);
		}
		//mode = sb.st_mode & RWX_UGO;		
		device->gps.mode = device->gps.sb.st_mode | RWX_UGO;		

		// Abre o arquivo para escrita
		device->gps.mmFileDescriptor=open(appconfig.mmSharedFile, O_CREAT|O_RDWR|O_TRUNC, device->gps.mode);
		if(device->gps.mmFileDescriptor < 0) {
			syslog(LOG_INFO, "==== DEBUG_MM: Não pôde abrir o arquivo %s!\n", appconfig.mmSharedFile);
			return EXIT_FAILURE;
		}
		syslog(LOG_INFO, "==== DEBUG_MM: Arquivo %s foi aberto com sucesso!\n", appconfig.mmSharedFile);

		device->gps.mem=NULL;
		ftruncate(device->gps.mmFileDescriptor, getpagesize());
		device->gps.mem=mmap(NULL, getpagesize(), PROT_READ|PROT_WRITE, MAP_SHARED, device->gps.mmFileDescriptor, 0);
		if(device->gps.mem == MAP_FAILED) {
			syslog(LOG_INFO, "==== DEBUG_MM: Não pôde mapear o endereço de memória do arquivo!\n");
			return EXIT_FAILURE;
		}
		syslog(LOG_INFO, "==== DEBUG_MM: Mapeamento virtual foi criado com sucesso!\n");
		syslog(LOG_INFO, "==== DEBUG_MM: Intervalo de compartillhamento %d segundos.\n", appconfig.sharingGpsDataInterval);

		//
		initialize();
		device->gps.timerMM = start_timer(appconfig.sharingGpsDataInterval, timerMMGPSHandler, TIMER_PERIODIC, &device->gps);


		for (;;) {
			#ifdef TIMERSERIAL
			if (device && device->flags.readyReadGPSData && !device->flags.connected) {
			#endif
			#ifndef TIMERSERIAL
			if (device && !device->flags.connected) {
			#endif
				syslog(LOG_INFO, "==== DEBUG_SERIAL: Tenta abrir a conexão serial com o GPS.\n");
				device->serialfd = open (appconfig.deviceSerialFilename, O_RDWR | O_NOCTTY | O_SYNC);
				if (device->serialfd < 0)
				{
					syslog(LOG_INFO, "==== DEBUG_SERIAL: Erro ao abrir a porta serial: %s - %s\n", 
						appconfig.deviceSerialFilename,
						strerror(errno)
					);
					fprintf(stderr, "==== DEBUG_SERIAL: Erro ao abrir a porta serial: %s - %s\n", 
						appconfig.deviceSerialFilename,
						strerror(errno)
					);
				} else {
					setSerialPortAttrs(device);
					device->flags.connected = 1;
					_timeReadSerialData = 0;
					if (pthread_create(&thSerial, &attr, pthSerialGPS, device) != 0) {
						syslog(LOG_INFO,"==== ERROR: %s: %s", "pthread_create", strerror(errno));
						close(device->serialfd);
						device->flags.connected = 0;
					}
					syslog(LOG_INFO, "==== DEBUG_SERIAL: Cria a Thread para processar o GPS.\n");
					device->pthreadid = thSerial;
				}
			} /*else {
				syslog(LOG_INFO, "==== DEBUG_MM: Escreve dados no arquivo %s.\n", appconfig.mmSharedFile);
			}//*/

			#ifdef TIMERSERIAL
			if (!device->flags.connected) {
				// contabiliza tempo
				_timeReadSerialData++;
				if (_timeReadSerialData >= appconfig.readGpsDataInterval) {
					_timeReadSerialData = 0;
					device->flags.readyReadGPSData = 1;
				}
				//syslog(LOG_INFO, "==== DEBUG ==== c[%d] r[%d]\n", appconfig.readGpsDataInterval, _timeReadSerialData);
			}
			#endif
			sleep(1);
		}
	} else {
		syslog(LOG_INFO, "==== ERROR: Não foi possível alocar memória para TGPSDevice.\n");
		munmap(device->gps.mem, getpagesize());
		finalize();
		return EXIT_FAILURE;
	}

	munmap(device->gps.mem, getpagesize());
	finalize();
	return EXIT_SUCCESS;
}

void showHelp( char *applicationname ) 
{
	syslog(LOG_INFO, "==== DEBUG: showHelp(%s).\n", applicationname);
	fprintf(stderr, "======== Help \n");
	fprintf(stderr, "\tVoce deve usar %s\n", applicationname);
	fprintf(stderr, "========\n");
}

void processArgs( int argc, char **argv, TApplicationConfiguration *appconfig )
{
	int opcao;
	int indice;

	memset(appconfig, 0, sizeof(TApplicationConfiguration));
	appconfig->debug.cfg = 1;
	while ((opcao = getopt (argc, argv, OPT_ARGS_OPTIONS)) != -1) {
		switch(opcao) {
			case 'd': {
				syslog(LOG_INFO, "==== DEBUG_CFG: Habilita o modo DEBUG \n");
			} break;

			case 'D': {
				if (strlen(argv[optind])>0) {
					strcpy(appconfig->deviceSerialFilename, argv[optind]);
					if(appconfig->debug.cfg == 1)
						syslog(LOG_INFO, "==== DEBUG_CFG: Device é %s \n", appconfig->deviceSerialFilename);
				} else {
					strcpy(appconfig->deviceSerialFilename, DEF_SERIALPORT_FILENAME);
				}
			} break;

			case 't': {
				appconfig->readGpsDataInterval = atoi(argv[optind]);
				if (appconfig->readGpsDataInterval == 0) appconfig->readGpsDataInterval = DEF_READ_GPSDATA_INTERVAL;
				if(appconfig->debug.cfg == 1)
					syslog(LOG_INFO, "==== DEBUG_CFG: Intervalo de leitura do GPS é %d \n", appconfig->readGpsDataInterval);
			} break;

			case 'i': {
				appconfig->gpsDataAcquisitionInterval = atoi(argv[optind]);
				if (appconfig->gpsDataAcquisitionInterval == 0) appconfig->gpsDataAcquisitionInterval = DEF_INTERVAL_ACQUISITION_SERIALDATA;
				if(appconfig->debug.cfg == 1)
					syslog(LOG_INFO, "==== DEBUG_CFG: Temo de aquisição dos dados GPS é %d segundos \n", appconfig->gpsDataAcquisitionInterval);
			} break;			

			case 'T': {
				appconfig->sharingGpsDataInterval = atoi(argv[optind]);
				if (appconfig->sharingGpsDataInterval == 0) appconfig->sharingGpsDataInterval = DEF_SHARING_GPSDATA_INTERVAL;
				if(appconfig->debug.cfg == 1)
					syslog(LOG_INFO, "==== DEBUG_CFG: Intervalo de compartilhamento do GPS é %d \n", appconfig->sharingGpsDataInterval);
			} break;

			case 'M': {
				if (strlen(argv[optind])>0) {
					strcpy(appconfig->mmSharedFile, argv[optind]);
					if(appconfig->debug.cfg == 1)
						syslog(LOG_INFO, "==== DEBUG_CFG: Arquivo de compartilhamento do Memory Mapping é %s \n", appconfig->mmSharedFile);
				} else {
					strcpy(appconfig->mmSharedFile, DEF_FILENAME_GPS_MEMORYMAPPING);
				}
			} break;

			case '?': {
				if(appconfig->debug.cfg == 1)
					syslog(LOG_INFO, "==== DEBUG_CFG: Opção ? \n");

				showHelp(argv[0]);
			} break;

			default: {
				if(appconfig->debug.cfg == 1)
					syslog(LOG_INFO, "==== DEBUG_CFG: OPTARGS Default");
			} break;
		}
	}

	/*syslog(LOG_INFO, "==== DEBUG_LOG: Outros argumentos que não são opções\n");
	for (indice = optind; indice < argc; indice++) {
		syslog(LOG_INFO, "==== DEBUG_LOG: ARG %d = %s\n", indice, argv[indice]);
	}//*/

	if (strlen(appconfig->deviceSerialFilename) == 0) {
		strcpy(appconfig->deviceSerialFilename, DEF_SERIALPORT_FILENAME);
	}

	if (strlen(appconfig->mmSharedFile) == 0) {
		strcpy(appconfig->mmSharedFile, DEF_FILENAME_GPS_MEMORYMAPPING);
	}

}


void timerMMGPSHandler(size_t timer_id, void * user_data)
{
	TGPS *gps;
	syslog(LOG_INFO, "==== DEBUG_MM: Handler!\n");
	if (user_data) {
		gps = (TGPS *) user_data;
		pthread_mutex_lock(&gps->mutexmm); 	// Protege região crítica
    	syslog(LOG_INFO, "==== DEBUG_MM: Escreve no arquivo.\n");
		
 		if (lseek (gps->mmFileDescriptor, 0, SEEK_SET) == -1) {
            syslog(LOG_INFO, "==== ERROR: Falha no lseek do arquivo %d !\n", gps->mmFileDescriptor);
        } else {
        	write(gps->mmFileDescriptor, gps, sizeof(TGPS));		
		}
		pthread_mutex_unlock(&gps->mutexmm); // Libera região crítica
	}
	
}