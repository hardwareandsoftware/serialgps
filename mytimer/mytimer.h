/** @file mytimer.h
 *  @brief Arquivo de cabeçalho do mytimer.c
 */ 
#ifndef MYTIME_H_
#define MYTIME_H_
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/timerfd.h>
#include <pthread.h>
#include <poll.h>
#include <stdio.h>
 

/** @enum t_timer
 *  Enumeration para definir o tipo de timer.
 */
typedef enum {
    TIMER_SINGLE_SHOT = 0, 
    TIMER_PERIODIC         
} t_timer;


 

typedef void (*time_handler)(size_t timer_id, void * user_data);
 
int initialize(void);

size_t start_timer(unsigned int interval, time_handler handler, t_timer type, void * user_data);

void stop_timer(size_t timer_id);

void finalize(void);

static void * _timer_thread(void * data);


#endif