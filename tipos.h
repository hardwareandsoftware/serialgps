/** @file tipos.h
 *  @brief Arquivo de cabeçalho de definição de tipos de dados
 */ 
#ifndef TIPOS_H_
#define TIPOS_H_

#define min(a,b)  ((a < b) ? a : b)
#define MIN(a,b)  ((a < b) ? a : b)

/** @typedef BYTE 
 *  Definição do tipo BYTE (8 bits) correspondente a: <strong>unsigned char<strong>
 */
typedef unsigned char BYTE;

/** @typedef WORD 
 *  Definição do tipo WORD correspondente a: <strong>unsigned int<strong>
 */
typedef unsigned int WORD;

/** @typedef DWORD 
 *  Definição do tipo DWORD correspondente a: <strong>unsigned long<strong>
 */
typedef unsigned long DWORD;

#endif // TIPOS_H_