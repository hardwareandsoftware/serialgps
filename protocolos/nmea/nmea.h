/** @file nmea.h
 *  @brief Arquivo de cabeçalho do nmea.c
 */ 
#ifndef NMEA_H_
#define NMEA_H_

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>

#define NMEA_MAX_SIZE_MESSAGE       512
#define NMEA_MAX_SIZE_STR_PARAM     25
#define NMEA_QTY_MAX_PARAMS         32
#define NMEA_SZ_CHECKSUM_PARAM      2

#define KMPH(a) (a*1.852f)

/* RMC */
#define POS_PAR_NMEA_GPRMC_TIME                 0
#define POS_PAR_NMEA_GPRMC_STATUS               1
#define POS_PAR_NMEA_GPRMC_LATITUDE             2
#define POS_PAR_NMEA_GPRMC_LATITUDE_SIGNAL      3
#define POS_PAR_NMEA_GPRMC_LONGITUDE            4
#define POS_PAR_NMEA_GPRMC_LONGITUDE_SIGNAL     5
#define POS_PAR_NMEA_GPRMC_SPEED                6
#define POS_PAR_NMEA_GPRMC_HEADING_DEGREES      7
#define POS_PAR_NMEA_GPRMC_DATE                 8
#define POS_PAR_NMEA_GPRMC_MAGNETIC_VARIATION   9
#define POS_PAR_NMEA_GPRMC_VARIATION_SIGNAL     10
#define POS_PAR_NMEA_GPRMC_CHECKSUM             11
/* GGA */
#define POS_PAR_NMEA_GPGGA_UTCTIME              0
#define POS_PAR_NMEA_GPGGA_LATITUDE             1
#define POS_PAR_NMEA_GPGGA_NSINDICATOR          2
#define POS_PAR_NMEA_GPGGA_LONGITUDE            3
#define POS_PAR_NMEA_GPGGA_EWINDICATOR          4
#define POS_PAR_NMEA_GPGGA_POSFIXINDICATOR      5
#define POS_PAR_NMEA_GPGGA_SATELLITES_USED      6
#define POS_PAR_NMEA_GPGGA_ALTITUDE             8


/** @enum TEnumNmeaSentence
    Descrição detalhada do TEnumNmeaSentence .
 */
typedef enum {
    NMEASENTENCE_NOSENTENCE=0,    
    NMEASENTENCE_GPRMC,         /**< Sentença RMC do NMEA - <strong>Recommended Minimum data</strong>*/ 
    NMEASENTENCE_GPGGA,         /**< Sentença GGA do NMEA */ 
    NMEASENTENCE_GPGSA,         /**< Sentença GSA do NMEA */ 
    NMEASENTENCE_GPGSV,         /**< Sentença GSV do NMEA */ 
    NMEASENTENCE_UNKNOWN        /**< Sentença não encontrada */ 
} TEnumNmeaSentence;

/** @enum TEnumNmeaState
    Enumeration que determina o estado do parser de recebimento da mensagem NMEA.
*/
typedef enum {
    NMEASTATE_WSTX,             /**< Aguarda início da sentença NMEA '$' */ 
    NMEASTATE_RCVSENTENCE,      /**< Recebendo dados do tipo de sentença NMEA (RMC, GGA, ...)*/ 
    NMEASTATE_RCVPARAMS,        /**< Recebendo paramêtros da sentença NMEA */ 
    NMEASTATE_RCVCHKSUM,        /**< Recebendo checksum da mensagem */ 
    NMEASTATE_UNKNOWN           
} TEnumNmeaState;


/** @struct TNmea
 *  É reponsável por armazenar e processar os dados NMEA recebidos do GPS pela serial .
 *  @author Tiago Sousa Rocha
 *  @brief Estrutura desenvolvida para receber os dados NMEA
 */
typedef struct {
    unsigned char data[NMEA_MAX_SIZE_MESSAGE+1]; /*!< Armazena os dados NMEA recebidos pela serial, exemplo: $GPRMC,....  */
    int sizeData; /*!< Armazena o tamanho total da mensagem recebida */
    int positionParser; /*!< Armazena a posição do parser utilizado no processamento da mensagem NMEA. */
    int positionField; /*!< Armazena a posição inicial do campo a ser processado pelo parser da mensagem NMEA. */
    int qtyDataParams; /*!< Armazena a quantidade de parâmetros recebidos na mensagem NMEA. */
    char dataparams[NMEA_QTY_MAX_PARAMS][NMEA_MAX_SIZE_STR_PARAM]; /*!< Armazena todos os parâmetros recebidos na mensagem NMEA. */
    unsigned int chksumRcv; /*!< Armazena o checksum recebido na mensagem. */
    unsigned int chksumCal; /*!< Armazena o checksum calculado da mensagem para comparar com o checksum recebido. */
    struct {
        unsigned validmsg : 1; 
    } flags;
    TEnumNmeaSentence sentence; /*!< Armazena o tipo de sentença NMEA (RMC, GGA e etc). */
} TNmea;

/** @function 
 *  Essa função converte o parametro date da sentença GPRMC, recebido pela serial, de string para struct tm.
 *  @author Tiago Sousa Rocha
 *  @param [in] const unsigned char *strDate
 *  @param [out] struct tm *datetime
 *  @return void
 */
void processesRMCDate(const unsigned char * strDate, struct tm *datetime);

/** @function 
 *  Essa função converte o parametro status da navegação da sentença GPRMC, recebido pela serial, de string para int. Se
 * válido, retornará 1 se é válido, e 0 (zero) se inválido.
 *  @author Tiago Sousa Rocha
 *  @param [in] const unsigned char status
 *  @return int
 */
int processesRMCStatusNavigation(const unsigned char status);

/** @function 
 *  Essa função converte o parametro time da sentença GPRMC, recebido pela serial, de string para struct tm.
 *  @author Tiago Sousa Rocha
 *  @param [in] const unsigned char *strTime
 *  @param [out] struct tm *datetime
 *  @return void
 */
void processesTime(const unsigned char * strTime, struct tm *datetime);

/** @function 
 *  Essa função converte o parametro de indicação de Norte e Sul na navegação da sentença GPRMC, recebido pela serial, de string para int. Se
 * válido, retornará 1 se é Norte, e 0 (zero) se Sul.
 *  @author Tiago Sousa Rocha
 *  @param [in] const unsigned char indicator
 *  @return int
 */
int processesNSIndicator(const unsigned char indicator);

/** @function 
 *  Essa função converte o parametro de indicação de Leste e Oeste na navegação da sentença GPRMC, recebido pela serial, de string para int. Se
 * válido, retornará 1 se é Leste, e 0 (zero) se Oeste.
 *  @author Tiago Sousa Rocha
 *  @param [in] const unsigned char indicator
 *  @return int
 */
int processesEWIndicator(const unsigned char indicator);

/** @function 
 *  Essa função converte o parametro de indicação de coordenada válida, recebido pela serial, de string para int. Se
 * válido, retornará:  0=Fix not available, 1=Fix valid, 2=2D fix, 3=3D fix.
 *  @author Tiago Sousa Rocha
 *  @param [in] const unsigned char posfix
 *  @return int
 */
int processesGGAPositionFixIndicator(const unsigned char posfix);

/** @function 
 *  Essa função converte o parametro de indicação de número de satélites, recebido pela serial, de string para int.
 *  @author Tiago Sousa Rocha
 *  @param [in] const unsigned char *strNoSV
 *  @return int
 */
int processesGGASatellitesUsed(const unsigned char * strNoSV);

/** @function 
 *  Essa função converte o parametro de indicação de direção (Course Over Ground), recebido pela serial, de string para int.
 *  @author Tiago Sousa Rocha
 *  @param [in] const unsigned char *cog
 *  @param [out] int *cogdst
 *  @return void
 */
void processesRMCCourseOverGround(const unsigned char * cog, int *cogdst);

/** @function 
 *  Essa função converte o parametro de indicação de velocidade, recebido pela serial, de string para int.
 *  @author Tiago Sousa Rocha
 *  @param [in] const unsigned char *speed
 *  @param [out] int *speeddst
 *  @return void
 */
void processesRMCSpeed(const unsigned char * speed, int * speeddst);

/** @function 
 *  Essa função converte o parametro Altitude, recebido pela serial, de string para inteiro.
 *  @author Tiago Sousa Rocha
 *  @param [in] const unsigned char *alt
 *  @param [out] int *altdst
 *  @return void
 */
void processesGGAAltitude(const unsigned char * alt, int *altdst);

/** @function 
 *  Essa função converte o parametro Latitude ou Longitude, recebido pela serial, de string para double, no formato Graus Decimais.
 *  @author Tiago Sousa Rocha
 *  @param [in] const unsigned char *coord
 *  @param [out] double *coorddst
 *  @return void
 */
void processesCoords(const unsigned char *coord, double *coorddst) ;

/** @function 
 *  Essa função converte o parametro CHECKSUM recebido pelo canal serial do GPS de string para int.
 *  @author Tiago Sousa Rocha
 *  @param [in] const unsigned char *chksum
 *  @return int
 */
int convertCHKSUM(const unsigned char *chksum /*!< Checksum a ser convertido em int */) ;

#endif // NMEA_H_