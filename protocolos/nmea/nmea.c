/*=============================================================================================
|
|       Author: Tiago Sousa Rocha
|
 ==============================================================================================*/
 #include "nmea.h"

 // TODO: Implementar o processamento da sentença GPGSA
 // TODO: Implementar o processamento da sentença GPGSV

const char * const * const arrStrNmeaSentence[] = {
	"NOSENTENCE",
	"GPRMC",
	"GPGGA",
	"GPGSA",
	"GPGSV",
	"UNKNOWN"
};

void processesRMCDate(const unsigned char * strDate, struct tm *datetime)
{
	unsigned char aux[7] = "";
	if (datetime && strDate) {
		if (strlen(strDate)==6) {
			// Dia
			strncpy(aux, &strDate[0], 2);
			aux[2]='\0';
			datetime->tm_mday = atoi(aux);
			// Mês
			strncpy(aux, &strDate[2], 2);
			aux[2]='\0';
			datetime->tm_mon = atoi(aux);
			// Ano
			strncpy(aux, &strDate[4], 2);
			aux[2]='\0';
			datetime->tm_year = atoi(aux);
		}
	}
}


void processesTime(const unsigned char * strTime, struct tm *datetime)
{
	unsigned char aux[7] = "";
	if (datetime && strTime) {
		if (strlen(strTime)>=6) {
			// Dia
			strncpy(aux, &strTime[0], 2);
			aux[2]='\0';
			datetime->tm_hour = atoi(aux);
			// Mês
			strncpy(aux, &strTime[2], 2);
			aux[2]='\0';
			datetime->tm_min = atoi(aux);
			// Ano
			strncpy(aux, &strTime[4], 2);
			aux[2]='\0';
			datetime->tm_sec = atoi(aux);
		}
	}
}

int processesRMCStatusNavigation(const unsigned char status)
{
	unsigned char st = 0;

	if (status == 'A') {
		st = 1;
	}
	return st;
}

int processesNSIndicator(const unsigned char indicator)
{
	unsigned char ns = 0;

	if (indicator == 'S') {
		ns = 1;
	}
	return ns;
}

int processesEWIndicator(const unsigned char indicator)
{
	unsigned char ew = 0;

	if (indicator == 'W') {
		ew = 1;
	}
	return ew;
}

int processesGGAPositionFixIndicator(const unsigned char posfix) 
{
	return atoi(&posfix);
}

int processesGGASatellitesUsed(const unsigned char * strNoSV)
{
	unsigned char aux[3] = "";
	if (strNoSV) {
		if (strlen(strNoSV)==2) {
			strncpy(aux, &strNoSV[0], 2);
			aux[2]='\0';
			return atoi(aux);
		}
	}
	return 0;
}

void processesRMCCourseOverGround(const unsigned char * cog, int *cogdst)
{
	if (cog) {
		if (strlen(cog)>=2) {
			*cogdst = (int)atof(cog);
		}
	}
}

void processesRMCSpeed(const unsigned char * speed, int * speeddst)
{
	if (speed) {
		if (strlen(speed)>=2) {
			*speeddst = (int)KMPH(atof(speed));
		}
	}
}

void processesGGAAltitude(const unsigned char * alt, int *altdst)
{
	if (alt) {
		if (strlen(alt)>=2) {
			*altdst = (int)atof(alt);
		}
	}
}

void processesCoords(const unsigned char *coord, double *coorddst) 
{
	int graus = 0;
	double aux = 0.0f;
	if (coord && coorddst && strlen(coord)>=8){
		aux = (atof(coord));
		graus = (int)(aux);
		graus = (int)graus/100.0f; 
		aux -= (graus * 100);
		aux = aux/60.0f;
		*coorddst = (graus+aux);
	}
}

int convertCHKSUM(const unsigned char *chksum) 
{
	unsigned char strHex[11];
	sprintf(strHex, "0x%s", chksum);
	return (int)strtol(strHex, NULL, 0);
}

