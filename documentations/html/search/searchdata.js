var indexSectionsWithContent =
{
  0: "abcdfglmnpqrstvw",
  1: "t",
  2: "gmnst",
  3: "cps",
  4: "acdfglmnpqrstv",
  5: "bdw",
  6: "t",
  7: "n",
  8: "d"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Pages"
};

