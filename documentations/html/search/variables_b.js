var searchData=
[
  ['satellites',['satellites',['../structTGPS.html#a6d61fcbbc9563bef9caf6484f31bc10a',1,'TGPS']]],
  ['sentence',['sentence',['../structTNmea.html#a060ace14ba0b10e245558f6806f6bb52',1,'TNmea']]],
  ['serialfd',['serialfd',['../structTGPSDevice.html#a629038ac34bfe13732220a98bd03a2d2',1,'TGPSDevice']]],
  ['serialport',['serialport',['../structTGPSDevice.html#a341da472a9cc68c27c5a521865af14d5',1,'TGPSDevice']]],
  ['serialportsettings',['serialportSettings',['../structTGPSDevice.html#ad7b92d34c916722db42bddce53a414e5',1,'TGPSDevice']]],
  ['serialstate',['serialstate',['../structTGPSDevice.html#a7498531966896ad9dbed6a3ba91430b2',1,'TGPSDevice']]],
  ['sharinggpsdatainterval',['sharingGpsDataInterval',['../structTApplicationConfiguration.html#a8b1b70f90b99c41925bfecf5ceab5dfd',1,'TApplicationConfiguration']]],
  ['sizedata',['sizeData',['../structTNmea.html#a40bfab78cd0cbf4975e1d896e024b7e9',1,'TNmea']]],
  ['speed',['speed',['../structTGPS.html#ac31faf7995137e0cd756cc216b79bf52',1,'TGPS']]]
];
