var searchData=
[
  ['satellites',['satellites',['../structTGPS.html#a6d61fcbbc9563bef9caf6484f31bc10a',1,'TGPS']]],
  ['sentence',['sentence',['../structTNmea.html#a060ace14ba0b10e245558f6806f6bb52',1,'TNmea']]],
  ['serialfd',['serialfd',['../structTGPSDevice.html#a629038ac34bfe13732220a98bd03a2d2',1,'TGPSDevice']]],
  ['serialgps_2ec',['serialgps.c',['../serialgps_8c.html',1,'']]],
  ['serialgps_2eh',['serialgps.h',['../serialgps_8h.html',1,'']]],
  ['serialport',['serialport',['../structTGPSDevice.html#a341da472a9cc68c27c5a521865af14d5',1,'TGPSDevice']]],
  ['serialportsettings',['serialportSettings',['../structTGPSDevice.html#ad7b92d34c916722db42bddce53a414e5',1,'TGPSDevice']]],
  ['serialstate',['serialstate',['../structTGPSDevice.html#a7498531966896ad9dbed6a3ba91430b2',1,'TGPSDevice']]],
  ['setserialportattrs',['setSerialPortAttrs',['../gps_8h.html#a8ad620075f7299f9dac207783cce211e',1,'gps.c']]],
  ['sharinggpsdatainterval',['sharingGpsDataInterval',['../structTApplicationConfiguration.html#a8b1b70f90b99c41925bfecf5ceab5dfd',1,'TApplicationConfiguration']]],
  ['showhelp',['showHelp',['../serialgps_8c.html#a25774c337b3a56a8031d88be3dafcd5c',1,'showHelp(char *applicationname):&#160;serialgps.c'],['../serialgps_8h.html#a25774c337b3a56a8031d88be3dafcd5c',1,'showHelp(char *applicationname):&#160;serialgps.c']]],
  ['signalapp_2eh',['signalapp.h',['../signalapp_8h.html',1,'']]],
  ['signalhandlersigalrm',['signalHandlerSIGALRM',['../signalapp_8h.html#a7d19cfc819450ba52f0785d188e0b161',1,'signalapp.c']]],
  ['signalhandlersigchld',['signalHandlerSIGCHLD',['../signalapp_8h.html#a32cc5c29fa1ea180adb5bc1605e00262',1,'signalapp.c']]],
  ['signalhandlersigint',['signalHandlerSIGINT',['../signalapp_8h.html#a2988819a166e925d151b95531d4ebe9d',1,'signalapp.c']]],
  ['signalhandlersigpipe',['signalHandlerSIGPIPE',['../signalapp_8h.html#a76f01f80c24c76e809e932b7405637e0',1,'signalapp.c']]],
  ['sizedata',['sizeData',['../structTNmea.html#a40bfab78cd0cbf4975e1d896e024b7e9',1,'TNmea']]],
  ['speed',['speed',['../structTGPS.html#ac31faf7995137e0cd756cc216b79bf52',1,'TGPS']]]
];
