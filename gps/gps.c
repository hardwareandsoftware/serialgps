/*=============================================================================================
|
|       Author: Tiago Sousa Rocha
|
 ==============================================================================================*/
 #include "gps.h"

static
TEnumNmeaSentence getNMEASentenceType(char *nmeatype)
{
	TEnumNmeaSentence type;
	char strType[SZ_MAXSTRING];
	int i;

	for (i = 0; nmeatype[i]; ++i) {
		strType[i] = toupper(nmeatype[i]);
	}
	strType[i] = '\0'; 
	for (type = NMEASENTENCE_NOSENTENCE; type < NMEASENTENCE_UNKNOWN; ++type) {
		if (!strcmp(arrStrNmeaSentence[type], strType)) break;
	}
	return type;
}

static 
void storeNMEASentenceType(TGPSDevice *device)
{
	TEnumNmeaSentence nmea;
	char strNmeaType[SZ_MAXSTRING];
	int sizeData;
	//syslog(LOG_INFO, "==== DEBUG: storaNMEASentenceType () .\n", strNmeaType);

	if (device->nmea.positionParser < device->nmea.positionField) return;
	sizeData = MIN((device->nmea.positionParser - device->nmea.positionField), NMEA_MAX_SIZE_STR_PARAM-1);
	strncpy(strNmeaType, device->nmea.data, sizeData);
	strNmeaType[sizeData] = '\0'; 
	nmea = getNMEASentenceType(strNmeaType);
	if (nmea != NMEASENTENCE_UNKNOWN) {
		device->nmea.sentence = nmea;
		device->nmea.positionField = device->nmea.positionParser + 1;
	}
}

static 
void storeNMEADataParam(TGPSDevice *device)
{
	int ns = 0;
	int sizeParam;

	// Armazena o parametro recebido
	if (device->nmea.qtyDataParams < NMEA_QTY_MAX_PARAMS) {
		if (device->nmea.positionParser - device->nmea.positionField > 0) {
			while (device->nmea.data[device->nmea.positionParser-ns-1] == GPSSER_SPACE) ++ns;	// Conta espaços após o parâmetro
			sizeParam = MIN((device->nmea.positionParser-device->nmea.positionField-ns), (NMEA_MAX_SIZE_STR_PARAM-1));
			strncpy(device->nmea.dataparams[device->nmea.qtyDataParams], &device->nmea.data[device->nmea.positionField], sizeParam);
		} else {
			sizeParam = 0;
		}
		device->nmea.dataparams[device->nmea.qtyDataParams][sizeParam] = '\0';
		++device->nmea.qtyDataParams;
	}
	device->nmea.positionField = device->nmea.positionParser + 1;
}


void clearParserGPSSerial(TGPSDevice *device)
{
	memset(&device->nmea, 0, sizeof(TNmea));	
	//memset(&device->gps, 0, sizeof(TGPS));		
	device->serialstate = E_GPSSERSTATE_WAIT_STX;
}

int parserMessageGPSSerial(TGPSDevice *device, BYTE data) 
{
    int res = 0;
    switch (device->serialstate) {
        case E_GPSSERSTATE_WAIT_STX: {
            if (data == GPSSER_STX) {
                device->nmea.sizeData = 0;
                device->serialstate = E_GPSSERSTATE_RECV_DATA;
            }
        } break;

        case E_GPSSERSTATE_RECV_DATA: {
            device->nmea.data[device->nmea.sizeData++] = data;
            if (data == GPSSER_ETX) {
                device->serialstate = E_GPSSERSTATE_PROCESS_NMEA;
                res = 1;
            }
        } break;

        default:
            break;
    }
    return res;
}


int parserNMEASentenceGPSSerial(TGPSDevice *device)
{
	unsigned char data;
	int ret = 0;

	for (device->nmea.positionParser = 0; 
			!ret && device->nmea.positionParser < device->nmea.sizeData; 
			++device->nmea.positionParser) {
		data = device->nmea.data[device->nmea.positionParser];
		switch (device->serialstate) {
			case E_GPSSERSTATE_PROCESS_NMEA: {
				device->nmea.chksumCal ^= data;
				if (data == GPSSER_COMMA) {
					storeNMEASentenceType(device);
					device->serialstate = E_GPSSERSTATE_RECV_PARAMS;
				}
			} break;

			case E_GPSSERSTATE_RECV_PARAMS: {
				if (data == GPSSER_SPACE) {
					if (device->nmea.positionParser == device->nmea.positionField) ++device->nmea.positionField;
					device->nmea.chksumCal ^= data;
				} else if (data == GPSSER_COMMA) {
					storeNMEADataParam(device);
					device->nmea.chksumCal ^= data;
				} else if (data == GPSSER_ETX) {
					storeNMEADataParam(device);
					device->nmea.chksumCal ^= data;
					ret = 1;
				} else if (data == GPSSER_CHKSUM) {
					device->serialstate = E_GPSSERSTATE_RECV_CHKSUM;
					device->nmea.positionField = device->nmea.positionParser + 1;
				} else {
					device->nmea.chksumCal ^= data;
				}
			} break;

			case E_GPSSERSTATE_RECV_CHKSUM: {
				if (data == GPSSER_ETX) {
					unsigned char aux[3];
					// Coleta Checksum
					strncpy(aux, &device->nmea.data[device->nmea.positionField], device->nmea.positionParser - device->nmea.positionField);
					aux[2]='\0';
					device->nmea.chksumRcv = convertCHKSUM(&aux);
					if (device->nmea.chksumCal != device->nmea.chksumRcv) {
						device->nmea.flags.validmsg = 0;
						syslog(LOG_INFO, "==== ERROR: Checksum inválido. (Rcv=%d) != (Calc=%d) \n", device->nmea.chksumRcv, device->nmea.chksumCal);
					} else {
						device->nmea.flags.validmsg = 1;
					}
					return 1;
				}
			} break;
		}
	}
	return ret;
}


void *pthSerialGPS(void *pDevice)
{
	TGPSDevice *device = (TGPSDevice *)pDevice;
	BYTE msg[SZ_MAXSTRING];
	ssize_t size;
	u_int32_t i;

	// 
	clearParserGPSSerial(device);
	//
	#ifdef TIMERSERIAL
	syslog(LOG_INFO, "==== TIMERSERIAL ==== (%d segundos)\n", device->gpsDataAcquisitionInterval);
	initialize();
	//
	device->timerReadGPSData = start_timer(device->gpsDataAcquisitionInterval, timerReadGPSDataHandler, TIMER_PERIODIC, device);
	#endif
	device->flags.readyReadGPSData = 1;

	syslog(LOG_INFO, "==== DEBUG_SERIAL: Prepara para ler os dados da serial. \n");
	#ifndef TIMERSERIAL
	while ((size = read(device->serialfd, msg, sizeof(msg))) > 0 && device->flags.connected) {
	#endif
	#ifdef TIMERSERIAL
	while ((size = read(device->serialfd, msg, sizeof(msg))) > 0 && device->flags.connected && device->flags.readyReadGPSData) {
	#endif
		for (i = 0; i < size; ++i) {
			if (parserMessageGPSSerial(device, msg[i]) == 1) {
				if (parserNMEASentenceGPSSerial(device) && device->nmea.flags.validmsg){
					if (device->nmea.sentence == NMEASENTENCE_GPRMC) {
						processesRMC(device);
					} else if (device->nmea.sentence == NMEASENTENCE_GPGGA) {
						processesGGA(device);
					} else if (device->nmea.sentence == NMEASENTENCE_GPGSA) {
						//processesGGA(device);
					} else if (device->nmea.sentence == NMEASENTENCE_GPGSV) {
						//processesGGA(device);
					}

					#ifdef DEBUG_GPS
					if (device->nmea.flags.validmsg == 1) {
						//syslog(LOG_INFO, "[%d] ============ GPS ============\n", device->pthreadid);
						syslog(LOG_INFO, "==== GPS ==== %02d/%02d/20%02d %02d:%02d:%02d [%02.06f, %03.06f] %d(m) %d(Km) %d(sv) %d(v) %d(COG) \n", 
							//device->pthreadid, 
							device->gps.datetime.tm_mday,
							device->gps.datetime.tm_mon,
							device->gps.datetime.tm_year,
							device->gps.datetime.tm_hour,
							device->gps.datetime.tm_min,
							device->gps.datetime.tm_sec,
							device->gps.latitude,
							device->gps.longitude,
							device->gps.altitude,
							device->gps.speed,
							device->gps.satellites,
							device->gps.flags.valid,
							device->gps.cog
						);
					}
					#endif
				
				}
				clearParserGPSSerial(device);
			}
		} // for
	}

FIM_CONEXAO:
	syslog(LOG_INFO, "==== DEBUG_SERIAL: Thread finalizando ...\n");
	#ifdef TIMERSERIAL
	stop_timer(device->timerReadGPSData);
	#endif
	device->flags.readyReadGPSData = 0;
	device->flags.connected = 0;
	close(device->serialfd);
	return NULL;
}

#ifdef TIMERSERIAL
void timerReadGPSDataHandler(size_t timer_id, void * user_data)
{
	TGPSDevice *device;
    syslog(LOG_INFO, "==== GPS ==== TimerReadGPSData\n");
	if (user_data) {
		device = (TGPSDevice *) user_data;
		pthread_mutex_lock(&device->mutexserial); 	// Protege região crítica
		device->flags.readyReadGPSData = !device->flags.readyReadGPSData;
	    syslog(LOG_INFO, "==== GPS ==== TimerReadGPSData (%d)\n", device->flags.readyReadGPSData);	
		pthread_mutex_unlock(&device->mutexserial); // Libera região crítica
	}
}
#endif

int processesRMC(TGPSDevice *device) 
{
	int ret = 0;
	// Processa a data
	processesRMCDate(device->nmea.dataparams[POS_PAR_NMEA_GPRMC_DATE], &device->gps.datetime);
	// Processa a hora
	processesTime(device->nmea.dataparams[POS_PAR_NMEA_GPRMC_TIME], &device->gps.datetime);
	// Processa Status
	device->gps.flags.valid = processesRMCStatusNavigation(device->nmea.dataparams[POS_PAR_NMEA_GPRMC_STATUS][0]);
	// Processa indicador de Norte/Sul
	device->gps.flags.ns_indicator = processesNSIndicator(device->nmea.dataparams[POS_PAR_NMEA_GPRMC_LATITUDE_SIGNAL][0]);
	// Processa indicador de Leste/Oeste
	device->gps.flags.ew_indicator = processesEWIndicator(device->nmea.dataparams[POS_PAR_NMEA_GPRMC_LONGITUDE_SIGNAL][0]);
	// Processa a direção (Course Over Ground - COG)
	processesRMCCourseOverGround(device->nmea.dataparams[POS_PAR_NMEA_GPRMC_HEADING_DEGREES], &device->gps.cog);
	// Processa a velocidade
	processesRMCSpeed(device->nmea.dataparams[POS_PAR_NMEA_GPRMC_SPEED], &device->gps.speed);
	// Processa Latitude
	processesCoords(device->nmea.dataparams[POS_PAR_NMEA_GPRMC_LATITUDE], &device->gps.latitude);
	if (device->gps.flags.ns_indicator) device->gps.latitude *= (-1);
	// Processa Longitude
	processesCoords(device->nmea.dataparams[POS_PAR_NMEA_GPRMC_LONGITUDE], &device->gps.longitude);
	if (device->gps.flags.ew_indicator) device->gps.longitude *= (-1);

	return ret;
}

int processesGGA(TGPSDevice *device) 
{
	int ret = 0;
	// Processa Status
	device->gps.flags.posfix = processesGGAPositionFixIndicator(device->nmea.dataparams[POS_PAR_NMEA_GPGGA_POSFIXINDICATOR][0]);
	// Processa a hora
	processesTime(device->nmea.dataparams[POS_PAR_NMEA_GPGGA_UTCTIME], &device->gps.datetime);
	// Processa indicador de Norte/Sul
	device->gps.flags.ns_indicator = processesNSIndicator(device->nmea.dataparams[POS_PAR_NMEA_GPGGA_NSINDICATOR][0]);
	// Processa indicador de Leste/Oeste
	device->gps.flags.ew_indicator = processesEWIndicator(device->nmea.dataparams[POS_PAR_NMEA_GPGGA_EWINDICATOR][0]);
	// Processa o número de satélites utilizados
	device->gps.satellites = processesGGASatellitesUsed(device->nmea.dataparams[POS_PAR_NMEA_GPGGA_SATELLITES_USED]);
	// Processa a Altitude
	processesGGAAltitude(device->nmea.dataparams[POS_PAR_NMEA_GPGGA_ALTITUDE], &device->gps.altitude);
	// Processa Latitude
	processesCoords(device->nmea.dataparams[POS_PAR_NMEA_GPGGA_LATITUDE], &device->gps.latitude);
	if (device->gps.flags.ns_indicator) device->gps.latitude *= (-1);
	// Processa Longitude
	processesCoords(device->nmea.dataparams[POS_PAR_NMEA_GPGGA_LONGITUDE], &device->gps.longitude);
	if (device->gps.flags.ew_indicator) device->gps.longitude *= (-1);
	
	return ret;
}


int processesGSA(TGPSDevice *device) 
{
	int ret = 0;

	return ret;
}


int setSerialPortAttrs(TGPSDevice *device) 
{
	memset (&device->serialportSettings, 0, sizeof(struct termios));

	if (tcgetattr (device->serialfd, &device->serialportSettings) != 0) {
		syslog(LOG_INFO, "==== ERROR: In tcgetattr errno: %d \n", errno);
		return -1;
	}
	// Configura o Baudrate
	cfsetispeed(&device->serialportSettings,B4800); /* Set Read  Speed as 4800                       */
	cfsetospeed(&device->serialportSettings,B4800); /* Set Write Speed as 4800                       */

	/* 8N1 Mode */
	device->serialportSettings.c_cflag &= ~PARENB;   /* Disables the Parity Enable bit(PARENB),So No Parity   */
	device->serialportSettings.c_cflag &= ~CSTOPB;   /* CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit */
	device->serialportSettings.c_cflag &= ~CSIZE;	 /* Clears the mask for setting the data size             */
	device->serialportSettings.c_cflag |=  CS8;      /* Set the data bits = 8                                 */
	device->serialportSettings.c_cflag &= ~CRTSCTS;       /* No Hardware flow Control                         */
	device->serialportSettings.c_cflag |= CREAD | CLOCAL; /* Enable receiver,Ignore Modem Control lines       */ 
	device->serialportSettings.c_iflag &= ~(IXON | IXOFF | IXANY);          /* Disable XON/XOFF flow control both i/p and o/p */
	device->serialportSettings.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG);  /* Non Cannonical mode                            */
	device->serialportSettings.c_oflag &= ~OPOST;/*No Output Processing*/

	/* Setting Time outs */
	device->serialportSettings.c_cc[VMIN] = 30; /* Read at least 10 characters */
	device->serialportSettings.c_cc[VTIME] = 0; /* Wait indefinetly   */

	if((tcsetattr(device->serialfd,TCSANOW,&device->serialportSettings)) != 0) /* Set the attributes to the termios structure*/
		syslog(LOG_INFO, "==== ERROR: In tcsetattr errno: %d \n", errno);

	tcflush(device->serialfd, TCIFLUSH);

	return 0;
}