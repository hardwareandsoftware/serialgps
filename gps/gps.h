/** @file gps.h
 *  @brief Arquivo de cabeçalho do gps.c
 */ 
#ifndef GPS_H_
#define GPS_H_
/*=============================================================================================
|
|       Author: Tiago Sousa Rocha
|
 ==============================================================================================*/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <syslog.h>
#include <sys/param.h>
#include <string.h>
#include <sys/types.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>

 #include "../tipos.h"
 #include "../protocolos/nmea/nmea.h"
 #include "../mytimer/mytimer.h"

#define SZ_MAXSTRING                1024
#define SZ_MAX_NMEA_SENTENCE_TYPE   6
#define QTY_MAX_NMEA_DATA_PARAMS    32

#define TST_SIZE_MAXSTRING          1024
#define TST_COUNT_NMEA				4

#define GPSSER_STX     '$'
#define GPSSER_ETX     '\n'
#define GPSSER_COMMA   ','
#define GPSSER_SPACE   ' '
#define GPSSER_CHKSUM  '*'

#define __USE_MISC


// Referências externas
extern const char * const * const arrStrNmeaSentence[];

/** @enum TEnumGPSSerialState
 *  Enumeration para o estado do parser de recebimento de dados do canal serial.
 */
typedef enum {
    E_GPSSERSTATE_WAIT_STX=0,
    E_GPSSERSTATE_RECV_DATA,
    E_GPSSERSTATE_PROCESS_NMEA,
    E_GPSSERSTATE_RECV_PARAMS,
    E_GPSSERSTATE_RECV_CHKSUM
} TEnumGPSSerialState;

 /** @struct TGPS
  *  @brief Estrutura de dados responsável por informar os dados geográficos de posicionamento GPS.
  *  @author Tiago Sousa Rocha
  */
 typedef struct {
    /* Data e hora */
    struct tm datetime; /*!< Armazena a data e a hora reportados pelo GPS */
    /* Latitude */
    double latitude;    /*!< Armazena a coordenada latitude (Y) reportado pelo GPS */
    /* Longitude */
    double longitude;   /*!< Armazena a coordenada longitude (X) reportado pelo GPS */
    /* Número de satélites */
    unsigned char satellites;   /*!< Armazena o número de satélites utilizados no cálculo GPS */
    /* Velocidade em km/h */
    int speed;  /*!< Armazena a velocidade em Km/h */
    /* Course over ground */
    int cog;    /*!< Armazena a direção em graus (degress) */
    /* Altitude in metros */
    int altitude;   /*!< Armazena a altitude em metros acima do nível do mar */

    /*  */
    struct {
        /* 1: Valid, 0: Is not valid */
        unsigned valid : 1;     /*!< Armazena a indicação de posicionamento válido ou inválido */
        /* 0: North, 1: South */
        unsigned ns_indicator : 1;
        /* 0: East, 1: West */
        unsigned ew_indicator : 1;
        /* 1=Fix Not available, 2=2D Fix, 3=3D Fix */
        unsigned posfix : 2;    /*!< Armazena a indicação de coordenada válida em 2D, 3D ou inválida */
    } flags;    /*!< Armazena o status do GPS */
    // Memory Mapping
    pthread_mutex_t mutexmm;
    int mmFileDescriptor;
    int *mem;
	struct stat sb;
    mode_t mode;
	ssize_t timerMM;
 } TGPS;

 /** @struct TGPSDevice
  *  @brief Estrutura de dados responsável por representar um dispositivo GPS Serial
  *  @author Tiago Sousa Rocha
  */
 typedef struct {
    int serialfd;                       /*!< Armazena o descritor do arquivo da porta serial */
    BYTE serialport[SZ_MAXSTRING+1];    /*!< Armazena o nome do arquivo da porta serial */
    TEnumGPSSerialState serialstate;    /*!< Armazena o estado do parser de recebimento de dados da serial */
    struct termios serialportSettings;  /*!< Armazena a configuração da porta serial */
    pthread_mutex_t mutexserial;        /*!< Mutex para controle de acesso concorrente */
    pthread_t pthreadid;                /*!< ID da thread da Serial */
    size_t timerReadGPSData;            /*!< Armazena o ID do timer de leitura da porta serial */
    int gpsDataAcquisitionInterval;
    struct {
        unsigned connected: 1;
        unsigned readyReadGPSData : 1;
    } flags;
    TNmea nmea; /*!< Armazena o objeto TNmea que contém os dados necessários para processar as coordenadas */
    TGPS gps;   /*!< Armazena o dados GPS já processados e prontos para compartilhar via Memory Mapping */

 } TGPSDevice;

/*==== Prototipagem de funções ====*/

/** @function 
 *  Essa função limpa os dados do TNmea evitando processamento de lixo.
 *  @author Tiago Sousa Rocha
 *  @param [in,out] TGPSDevice *device
 *  @return void
 */
void clearParserGPSSerial(TGPSDevice *device);

/** @function 
 *  Essa função faz o primeiro processamento da mensagem recebida pela serial, character por character.
 *  @author Tiago Sousa Rocha
 *  @param [in,out] TGPSDevice *device
 *  @param [in] BYTE dado
 *  @return int
 */
int parserMessageGPSSerial(TGPSDevice *device, BYTE dado);

/** @function 
 *  Essa função faz o segundo processamento da mensagem recebida pela serial, armazenando e processando os parâmetros.
 *  @author Tiago Sousa Rocha
 *  @param [in,out] TGPSDevice *device
 *  @return int
 */
int parserNMEASentenceGPSSerial(TGPSDevice *device);

/** @function 
 *  Essa função armazena o tipo de sentença NMEA (RMC, GGA etc) recebido pela serial.
 *  @author Tiago Sousa Rocha
 *  @param [in,out] TGPSDevice *device
 *  @return void
 */
static void storeNMEASentenceType(TGPSDevice *device);

/** @function 
 *  Essa função converte o tipo de sentença NMEA (RMC, GGA etc) de string para o tipo
 *  correspondente no enumeration TEnumNmeaSentence
 *  @author Tiago Sousa Rocha
 *  @param [in] char *nmeatype
 *  @return TEnumNmeaSentence
 */
static TEnumNmeaSentence getNMEASentenceType(char *nmeatype);

/** @function 
 *  Essa função faz o processamento da mensagem RMC, armazenando e processando os parâmetros.
 *  @author Tiago Sousa Rocha
 *  @param [in,out] TGPSDevice *device
 *  @return int
 */
int processesRMC(TGPSDevice *device);

/** @function 
 *  Essa função faz o processamento da mensagem GGA, armazenando e processando os parâmetros.
 *  @author Tiago Sousa Rocha
 *  @param [in,out] TGPSDevice *device
 *  @return int
 */
int processesGGA(TGPSDevice *device);

/** @function 
 *  Essa função é a thread de processamento da porta serial.
 *  @author Tiago Sousa Rocha
 *  @param [in] void *pDevice
 *  @return void *
 */
void *pthSerialGPS(void *pDevice /*!< Passa o endereço(ponteiro) do TGPSDevice */);

/** @function 
 *  Essa função faz a configuração da porta serial.
 *  @author Tiago Sousa Rocha
 *  @param [in] TGPSDevice *device
 *  @return int
 */
int setSerialPortAttrs(TGPSDevice *device) ;

#ifdef TIMERSERIAL
/** @function 
 *  Essa função é o handler do timer da porta serial, habilitando-a a receber e processar os dados.
 *  @author Tiago Sousa Rocha
 *  @param [in] size_t timer_id
 *  @param [in] void * user_data
 *  @return void
 */
void timerReadGPSDataHandler(size_t timer_id, void * user_data);
#endif

#endif // GPS_H_